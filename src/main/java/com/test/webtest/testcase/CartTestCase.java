package com.test.webtest.testcase;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import com.test.webtest.ui.CartPage;
import com.test.webtest.ui.HeaderMenu;
import com.test.webtest.ui.ShopPage;
import com.test.webtest.ui.SignInPage;

import junit.framework.Assert;

public class CartTestCase {

	private WebDriver driver;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/lib/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@After
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void test_cart() {
		// open URL
		openSite("http://automationpractice.com/index.php");

		// click "dress > summer dress"
		HeaderMenu headerMenu = PageFactory.initElements(driver, HeaderMenu.class);
		ShopPage shopPage = PageFactory.initElements(driver, ShopPage.class);
		CartPage cartPage = PageFactory.initElements(driver, CartPage.class);
		SignInPage signinPage = PageFactory.initElements(driver, SignInPage.class);

		headerMenu.hoverDressMenu();
		headerMenu.clickSummerDress();

		// add items. Add the first and third item to the cart
		shopPage.addToCart(1);
		shopPage.addToCart(3);

		// goto cart and verify item display correctly in the cart
		headerMenu.gotoCart();

		List<String> itemNames = cartPage.getAllItemNames();
		Assert.assertEquals("Printed Summer Dress", itemNames.get(0));
		Assert.assertEquals("Printed Chiffon Dress", itemNames.get(1));

		cartPage.continueShopping();

		// goto cart
		headerMenu.gotoCart();

		// click "proceed to checkout", goto "sign in" page
		cartPage.gotoCheckout();

		// assert sign in page presented
		signinPage.clickSignIn();

		System.out.println("Finish");

	}

	private void openSite(String url) {
		driver.get(url);

		// maximize window
		driver.manage().window().maximize();
	}

}
