package com.test.webtest.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ShopPage extends BasePage {

	public ShopPage(WebDriver dirver) {
		super(dirver);
	}

	/**
	 * 
	 * @param index item index, start form 1
	 */
	public void addToCart(int index) {
		                
		By ele = By.xpath("//*[@id=\"center_column\"]/ul/li[" + index + "]/div/div[2]");
		super.click(super.findElement(ele));

		ele = By.xpath("//*[@id=\"center_column\"]/ul/li[" + index + "]/div/div[2]/div[2]/a[1]/span");
		super.click(super.findElement(ele));

		ele = By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span");
		super.click(super.findElement(ele));
	}

}
