package com.test.webtest.ui;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends BasePage {

	@FindBy(xpath = "//*[@id=\"center_column\"]/p[2]/a[1]/span")
	private WebElement checkoutBtn;

	@FindBy(xpath = "//*[@id=\"center_column\"]/p[2]/a[2]")
	private WebElement continueShoppingBtn;

	@FindBy(xpath = "//*[@id=\"cart_summary\"]/tbody/tr/td[2]/p/a")
	private List<WebElement> itemNames;

	public CartPage(WebDriver dirver) {
		super(dirver);
	}

	public void gotoCheckout() {
		super.click(checkoutBtn);
	}

	public void continueShopping() {
		super.click(continueShoppingBtn);
	}

	public List<String> getAllItemNames() {

		List<String> result = new ArrayList<String>(itemNames.size());
		for (WebElement item : itemNames) {
			result.add(item.getText());
		}

		return result;
	}

}
