package com.test.webtest.ui;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {

	private WebDriver driver;
	private WebDriverWait wait;
	private Actions action;

	public BasePage(WebDriver dirver) {
		this.driver = dirver;
		this.wait = new WebDriverWait(driver, 20);
		this.action = new Actions(driver);
	}

	protected WebElement findElement(By by) {
		WebElement r = driver.findElement(by);
		return r;
	}

	protected List<WebElement> findElements(By by) {
		List<WebElement> r = driver.findElements(by);
		return r;
	}

	protected void waitForClick(WebElement ele) {
		wait.until(ExpectedConditions.elementToBeClickable(ele));
	}

	protected void hover(WebElement ele) {
		waitForClick(ele);
		action.moveToElement(ele).perform();
	}

	protected void click(WebElement ele) {
		waitForClick(ele);
		ele.click();
	}

}
