package com.test.webtest.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

	@FindBy(xpath = "//*[@id=\"SubmitLogin\"]/span")
	private WebElement signinBtn;

	public SignInPage(WebDriver dirver) {
		super(dirver);
	}

	public void clickSignIn() {
		super.click(signinBtn);
	}

}
