package com.test.webtest.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HeaderMenu extends BasePage {

	@FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[2]")
	private WebElement dressMenu;

	@FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[2]/ul/li[3]/a")
	private WebElement summerDress;

	@FindBy(xpath = "//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a")
	private WebElement cartButton;

	public HeaderMenu(WebDriver dirver) {
		super(dirver);
	}

	public void hoverDressMenu() {
		super.hover(this.dressMenu);
	}

	public void clickSummerDress() {
		super.click(this.summerDress);
	}

	public void gotoCart() {
		super.click(this.cartButton);
	}

}
